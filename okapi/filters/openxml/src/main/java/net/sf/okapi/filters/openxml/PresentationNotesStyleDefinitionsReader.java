/*
 * =============================================================================
 *   Copyright (C) 2010-2019 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndDocument;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartDocument;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.Reader;
import java.util.Arrays;
import java.util.List;

class PresentationNotesStyleDefinitionsReader implements StyleDefinitionsReader {
    private static final String NOTES_STYLE = "notesStyle";
    private static final String DEF_RPR = "defRPr";
    private static final String UNEXPECTED_STRUCTURE = "Unexpected notes style structure: ";

    private final ConditionalParameters conditionalParameters;
    private final XMLInputFactory inputFactory;
    private final XMLEventFactory eventFactory;
    private final Reader reader;
    private final List<String> paragraphLevels;
    private final Cache cache;

    PresentationNotesStyleDefinitionsReader(
        final ConditionalParameters conditionalParameters,
        final XMLInputFactory inputFactory,
        final XMLEventFactory eventFactory,
        final Reader reader
    ) {
        this.conditionalParameters = conditionalParameters;
        this.inputFactory = inputFactory;
        this.eventFactory = eventFactory;
        this.reader = reader;
        this.paragraphLevels = Arrays.asList(
            "lvl1pPr",
            "lvl2pPr",
            "lvl3pPr",
            "lvl4pPr",
            "lvl5pPr",
            "lvl6pPr",
            "lvl7pPr",
            "lvl8pPr",
            "lvl9pPr"
        );
        this.cache = new Cache();
    }

    @Override
    public StartDocument readStartDocument() throws XMLStreamException {
        if (this.cache.hasStartDocument()) {
            return this.cache.startDocument;
        }
        this.cache.eventReader = this.inputFactory.createXMLEventReader(reader);
        while (this.cache.eventReader.hasNext()) {
            final XMLEvent event = this.cache.eventReader.nextEvent();
            if (event.isStartDocument()) {
                this.cache.startDocument = (StartDocument) event;
                return this.cache.startDocument;
            }
        }
        throw new IllegalStateException(UNEXPECTED_STRUCTURE.concat("the start document event is absent"));
    }

    private boolean hasStartElement() throws XMLStreamException {
        if (!this.cache.hasEventReader()) {
            readStartDocument();
        }
        if (this.cache.hasStartElement()) {
            return true;
        }
        while (this.cache.eventReader.hasNext()) {
            final XMLEvent event = this.cache.eventReader.peek();
            if (event.isStartElement() && NOTES_STYLE.equals(event.asStartElement().getName().getLocalPart())) {
                return true;
            }
            this.cache.eventReader.nextEvent();
        }
        return false;
    }

    @Override
    public StartElement readStartElement() throws XMLStreamException {
        if (!this.cache.hasEventReader()) {
            readStartDocument();
        }
        if (this.cache.hasStartElement()) {
            return this.cache.startElement;
        }
        while (this.cache.eventReader.hasNext()) {
            final XMLEvent event = this.cache.eventReader.nextEvent();
            if (event.isStartElement() && NOTES_STYLE.equals(event.asStartElement().getName().getLocalPart())) {
                this.cache.startElement = event.asStartElement();
                return this.cache.startElement;
            }
        }
        throw new IllegalStateException(UNEXPECTED_STRUCTURE.concat("the start element is absent"));
    }

    PresentationNotesStyleDefinition readDocumentDefaults() throws XMLStreamException {
        if (!this.cache.hasEventReader()) {
            if (hasStartElement()) {
                readStartElement();
            } else {
                return new PresentationNotesStyleDefinition.Empty();
            }
        }
        if (this.cache.hasDocumentDefaults()) {
            return this.cache.documentDefaults;
        }
        while (this.cache.eventReader.hasNext()) {
            final XMLEvent event = this.cache.eventReader.nextEvent();
            if (event.isStartElement() && event.asStartElement().getName().getLocalPart().equals(DEF_RPR)) {
                this.cache.documentDefaults = new PresentationNotesStyleDefinition.ParagraphDefault(
                    this.conditionalParameters,
                    this.eventFactory,
                    event.asStartElement()
                );
                this.cache.documentDefaults.readWith(this.cache.eventReader);
                return this.cache.documentDefaults;
            }
            if (event.isStartElement() && this.paragraphLevels.contains(event.asStartElement().getName().getLocalPart())) {
                this.cache.paragraphStyleStartElement = event.asStartElement();
                break;
            }
            if (event.isEndElement() && event.asEndElement().getName().getLocalPart().equals(NOTES_STYLE)) {
                break;
            }
        }
        return new PresentationNotesStyleDefinition.Empty();
    }

    boolean hasNextParagraphLevel() throws XMLStreamException {
        if (!this.cache.hasEventReader()) {
            readDocumentDefaults();
        }
        if (this.cache.hasParagraphStyleStartElement()) {
            return true;
        }
        while (this.cache.eventReader.hasNext()) {
            final XMLEvent event = this.cache.eventReader.peek();
            if (event.isStartElement() && this.paragraphLevels.contains(event.asStartElement().getName().getLocalPart())) {
                return true;
            }
            if (event.isEndElement() && NOTES_STYLE.equals(event.asEndElement().getName().getLocalPart())) {
                this.cache.eventReader.nextEvent();
                return false;
            }
            this.cache.eventReader.nextEvent();
        }
        return false;
    }

    PresentationNotesStyleDefinition readNextParagraphLevel() throws XMLStreamException {
        if (!this.cache.hasEventReader()) {
            readDocumentDefaults();
        }
        if (this.cache.hasParagraphStyleStartElement()) {
            final PresentationNotesStyleDefinition styleDefinition = presentationNotesStyleDefinition(this.cache.paragraphStyleStartElement);
            styleDefinition.readWith(this.cache.eventReader);
            this.cache.invalidateParagraphStyleStartElement();
            return styleDefinition;
        }
        while (this.cache.eventReader.hasNext()) {
            final XMLEvent event = this.cache.eventReader.nextEvent();
            if (event.isStartElement() && this.paragraphLevels.contains(event.asStartElement().getName().getLocalPart())) {
                final PresentationNotesStyleDefinition styleDefinition = presentationNotesStyleDefinition(event.asStartElement());
                styleDefinition.readWith(this.cache.eventReader);
                return styleDefinition;
            }
            if (event.isEndElement() && NOTES_STYLE.equals(event.asEndElement().getName().getLocalPart())) {
                return new PresentationNotesStyleDefinition.Empty();
            }
        }
        throw new IllegalStateException(UNEXPECTED_STRUCTURE.concat("the notes-style-start and notes-style-end elements are absent"));
    }

    private PresentationNotesStyleDefinition presentationNotesStyleDefinition(final StartElement paragraphStyleStartElement) {
        return new PresentationNotesStyleDefinition.ParagraphLevel(
            new PresentationNotesStyleDefinition.ParagraphDefault(
                this.conditionalParameters,
                this.eventFactory,
                paragraphStyleStartElement
            )
        );
    }

    @Override
    public EndElement readEndElement() throws XMLStreamException {
        throw new UnsupportedOperationException();
    }

    @Override
    public EndDocument readEndDocument() throws XMLStreamException {
        throw new UnsupportedOperationException();
    }

    static class Cache {
        private XMLEventReader eventReader;
        private StartDocument startDocument;
        private StartElement startElement;
        private PresentationNotesStyleDefinition documentDefaults;
        private StartElement paragraphStyleStartElement;

        Cache() {
        }

        boolean hasEventReader() {
            return null != this.eventReader;
        }

        boolean hasStartDocument() {
            return null != this.startDocument;
        }

        boolean hasStartElement() {
            return null != this.startElement;
        }

        boolean hasDocumentDefaults() {
            return null != this.documentDefaults;
        }

        boolean hasParagraphStyleStartElement() {
            return null != this.paragraphStyleStartElement;
        }

        void invalidateParagraphStyleStartElement() {
            this.paragraphStyleStartElement = null;
        }
    }
}
